
server_addr = "localhost"
server_port = 4242
scanner_dev = "flatbed scanner"
gpio_button_pin = 10
gpio_led_pin = 12
delay_secs_after_scan = 3

# set any of these to False for testing
use_scanner = False
use_gpio = False
use_network = True

