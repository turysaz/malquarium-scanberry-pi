# Malquarium ScanberryPi

This repository contains the scanner/client side code for the Malquarium.
It can also be used to test the server by just sending test data.

## Dependencies

* `zbar`
* `pyzbar`
    * included as git submodule
* `python-sane`
* `python3-rpi`

## Configuration and usage

Configure the malquarium in with the `config.py`.
For testing purposes, scanner, gpio and network/SLTP can be switched
off individually.
For example, to test the server, turn GPIO and scanner off to only
use the sending of test images.

Run

    ./run.sh

to run the application.

In "production", you probably want to run this headless, so don't
forget to use `screen`.

